from flask import Flask
from pmdata import *
app = Flask(__name__)

apiUrl = "https://maccas-api.herokuapp.com/api"


@app.route('/')
def get_traces():
    return str(find_traces(make_request(apiUrl)))


if __name__ == '__main__':
    app.run()
