import requests
from collections import Counter


def make_request(url):
    return requests.get(url)


def find_traces(req):
    return [tuple(map(lambda y: y['activity'], x)) for x in [
            trace['activities'] for trace in req.json()]]


def find_transitions(variants):
    return [item for sublist in [tuple(zip(x[0:-1], x[1:])) for x in variants]
            for item in sublist]


if __name__ == "__main__":
    data = find_traces(make_request("https://maccas-api.herokuapp.com/api"))
    # print(data)
    print(str(data))
    # print(Counter(find_transitions(data)))
